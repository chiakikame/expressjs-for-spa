# How to: serve SPA within your API Server

This application is for demonstration of putting API service and SPA (Single Page Application) serving mechanism together.

## Usage

Assume your SPA has such structure

```
/path/to/spa
├ index.html
└ static
  ├ asset1.js
  ├ ...
  └ style.css
```

Then you may serve the SPA with the following commands

```bash
# Install dependencies
yarn install
# Serve the SPA
yarn start /path/to/spa 8080
```

In default settings, the program assumes SPA will be in `__dirname + /app`, and default port is 3000.

## Theory

Ok, let's talk about how to make express be able to serve SPA.

For normal servers, the problem of serving SPA is, if you request a resource by entering a URL you get from the SPA application, you would get a 404 response. Why ? Because by entering the URL on the browser, you directly make a request to the server instead of the internal router of your SPA. For example:

Since it's impossible to let your SPA process the URL you entered into the browser, we should make modifications to the server so that it can serve SPA.

If you want to serve SPA without error (i.e. no-404), you should configure your serve, so that when it receives requests containing URL handled by your SPA, it should send index HTML page (i.e. `index.html`) to the user without modifying the URL path (i.e. no redirection).

That is, if `/users/1485738` is a URL from SPA, the server should send content of `index.html` as response. After browser received the response, internal router of your SPA will handle the path.

If you want to bundle your API service inside the same server, the server should check if a request is for accessing an API resource, then determine if it's a path handled by SPA.

## Implementation

In this implementation, request URL checking rule is as the following orders:

* If it's an API call, process it with API handler.
* If it's a static resource request, send corresponding resource.
* For rest of the cases, send `index.html`.

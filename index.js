// Serving SPA with express.js
// With no additional dependencies!
//
// imports
const express = require('express');
const util = require('util');
const os = require('os');
const path = require('path');

// settings
const app = express();
const rootDir = process.argv[2] ? process.argv[2] : path.join(__dirname, 'app');
const port = process.argv[3] ? parseInt(process.argv[3]) : 3000;

// computed variables
const rootHtml = path.join(rootDir, 'index.html');
const staticDir = path.join(rootDir, 'static');

console.log(`SPA directory is assigned to ${rootDir}`);
console.log(`SPA html page is assumed to be in ${rootHtml}`);
console.log(`Static assets are assumed to be in ${staticDir}`);

// routes & middlewares
app.get('/api/test', function(req, res) {
  console.log({mode: 'api', path: req.path, event: 'API accessed'});
  res.send({message: 'Hello world!'});
});

app.use('/static', express.static(staticDir));
app.use(spaCached(rootHtml));
app.listen(port);

console.log(`http://localhost:${port}/`);

// Helper
// Simple SPA, reads the html page every time when it thinks it's a
// request for SPA.
function spa(htmlPage) {
  return function middleware(req, res, next) {
    console.log({mode: 'direct-spa', path: req.path, event: 'send-direct'});
    res.sendFile(htmlPage);
  };
}

// Cached SPA, sends cached SPA page.
// It will try to reload buffer if a change is detected on the page.
function spaCached(htmlPage) {
  const fs = require('fs');
  let buffer = null;

  function readFile() {
    fs.readFile(htmlPage, (err, _buffer) => {
      if (err) throw err;
      console.log({mode: 'cached-spa', event: 'cache-build-ok'});
      buffer = _buffer;
    });
  }
  console.log({mode: 'cached-spa', event: 'first-cache'});
  readFile();

  fs.watch(htmlPage, (eventType, fn) => {
    console.log({mode: 'cached-spa', event: 're-cache'});
    readFile();
  });

  return function middleware(req, res, next) {
    if (buffer) {
      console.log({mode: 'cached-spa', path: req.path, event: 'send-cached'});
      res.set('Content-Type', 'text/html');
      res.send(buffer);
    } else {
      console.log({mode: 'cached-spa', path: req.path, event: 'send-direct'});
      res.sendFile(htmlPage);
    }
  };
}
